# 3hrs of Pain - Why you should never commit your AWS Keys into GitHub

#### Name: Mike Mackay
#### Company: AWS
#### Job Title: Solutions Architect
#### Talk format: Lightning Talk
#### Level of talk: Introductory

### Elevator pitch

What really happens when you commit AWS keys into GitHub? We found out, and it wasn't pleasant.

### In-depth pitch

We all know that we shouldn't commit AWS keys into GitHub. Here, we learn how credential theft can lead to account takeovers, many unwarranted EC2 instances and pain.
We'll show how we stopped the problem along with some easy-to-implement mitigation tactics and alerts.

### What will attendees will take away from your talk?

- Why committing keys is a bad idea(!)
- How to stop an account takeover from continuing
- Migitation tactics to prevent it happening
- How AWS services can alert us in the event it happens

### Other information

- Region: UKI / EMEA
- Speaker cert (internal only): No
- Speaker bio: Mike is a Solutions Architect at AWS with a 10+ year background in Web Development. He has a passion all things tech and loves to share knowledge, insight and engage with the Web community.
- Social media links: twitter: @mikemackay
