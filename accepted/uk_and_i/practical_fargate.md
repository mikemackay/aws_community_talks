# Practical Fargate

#### Name: Ric Harvey
#### Company: AWS
#### Job Title: Technical Evangelist
#### Talk format: Live Demo
#### Level of talk: Advanced

### Elevator pitch

A zero slides demo to show how to launch services into Fargate.

### In-depth pitch

Fargate can run your containers and remove the heavy lifting of infrastructure management, allowing you to focus on the application development and innovation. This demo will show you a practical example how to leverage the technology and integrate this into you work flow and deployment pipelines. All the code and files needed to run this demo are available on GitLab and will be given to all attendees so you can this at home and integrate into your projects.

### What will attendees will take away from your talk?

- Understanding of the Fargate technology
- Practical knowledge of how to integrate with a CI/CD pipeline
- Working code to try yourself

### Other information

- Region: World Wide
- Speaker cert (internal only): Yes
- Speaker bio: Ric is a Technical Evangelist at AWS. He has a long career in operations and development using cloud technologies. Coupled with his passion for community and open source, he enjoys sharing this knowledge and engaging with audiences.
- Social media links: twitter: @ric__harvey gitlab: https://gitlab.com/ric_harvey
