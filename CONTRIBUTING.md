# Contributing

## What is this?
This repository allows you to submit an idea you may have for a community talk about AWS technologies. These talks will be made available to the AWS Community Leaders, who will request you talks by liasing with the local evangelist for that region.

## How to contribute

### Submit a talk

To submit a talk you'll need to exercise your markdown and git skills:

- fork the repository here on gitlab
- create a new file from the template (template.md), please give it he name of your talk (e.g. practical_fargate.md)
- fill in all the details including the regions you can deliver the content
- submit a [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)

### Vote up a talk

- look at the merge requests
- add a comment and +1