# ```Name of Talk```

#### Name: ```Your name``` (and alias if internal)
#### Company: ```Company```
#### Job Title: ```Job Title```
#### Talk format: ```Lightning talk/Live Demo/1 Hour session/Chalk Talk/Panel/Other```
#### Level of talk: ```introductory/advanced/expert```

### Elevator pitch

```brief synopsis of your talk, in one sentance```

### In-depth pitch

```outline of your talk max 250 words```

### What will attendees will take away from your talk?

```list key take aways max 100 words```

### Other information

- Region: ```REGION``` (where can you travel to deliver this talk?)
- Speaker cert (internal only): ```Yes/No```
- Speaker bio: ```your bio```
- Social media links: ```links to twitter etc```
